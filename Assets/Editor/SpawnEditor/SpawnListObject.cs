﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class SpawnListObject
{
  public struct EditorParam
  {
    public string name;
    public Sprite sprite;
    public Rect rect;

    public EditorParam(GameObject gameObject)
    {
      name = gameObject.name;
      sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
      rect = new Rect();
      rect.center = gameObject.transform.position;
      rect.size = gameObject.transform.localScale;
    }
  }

  GameObject[] gameObjects;

  public SpawnListObject()
  {
    Sync();
  }

  public void RemoveGameObjectAt(int index)
  {
    var list = gameObjects.ToList();
    list.RemoveAt(index);
    gameObjects = list.ToArray();
  }

  public void AddGameObject()
  {
    SpawnObject.CreateSpawnObject();
    Sync();
  }

  public GameObject GetGameObjectAt(int index)
  {
    return gameObjects == null || index < 0 || index > (gameObjects.Count() - 1) ?
      null : gameObjects[index];
  }

  public EditorParam GetEditorParamAt(int index)
  {
    return new EditorParam(GetGameObjectAt(index));
  }

  public EditorParam[] GetEditorParams()
  {
    List<EditorParam> paramList = new List<EditorParam>();
    int index = 0;
    foreach (var obj in gameObjects) {
      paramList.Add(GetEditorParamAt(index));
      index++;
    }
    return paramList.ToArray();
  }

  public void Sync()
  {
    var list = new List<GameObject>();
    for(var i = 0; i < SpawnObject.GetSpawnObjectCount(); i++) {
      list.Add(SpawnObject.GetSpawnObjectAt(i).gameObject);
    }
    gameObjects = list.ToArray();
  }
}
