﻿using UnityEngine;
using System.Collections;

public class MapObject
{
  const string TAG = "Map";

  GameObject gameObject;

  public MapObject()
  {
    gameObject = GameObject.FindWithTag(TAG);
  }

  private GameObject CreateGameObject()
  {
    var obj = new GameObject("Map Background Image");
    obj.AddComponent<SpriteRenderer>();
    obj.tag = TAG;
    return obj;
  }

  public GameObject GetGameObject()
  {
    if (gameObject == null) {
      gameObject = CreateGameObject();
    }
    return gameObject;
  }

  public Sprite GetSprite()
  {
    return GetGameObject().GetComponent<SpriteRenderer>().sprite;
  }

  public void SetSprite(Sprite sprite)
  {
    GetGameObject().GetComponent<SpriteRenderer>().sprite = sprite;
  }
}
