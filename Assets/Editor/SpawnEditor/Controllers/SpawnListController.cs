﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SpawnListController
{
  public delegate void EditorParamUpdateEventHandler(SpawnListObject.EditorParam param, int index);
  public delegate void GameObjectSelectEventHandler(int index);
  public delegate void SimpleEventHandler();
  public event EditorParamUpdateEventHandler OnParamUpdated;
  public event SimpleEventHandler OnClickAddGameObjectButton;
  public event GameObjectSelectEventHandler OnClickSelectGameObjectButton;
  public event GameObjectSelectEventHandler OnClickRemoveGameObjectButton;
  public event GameObjectSelectEventHandler OnSelectedIndexChanged;

  static int checkRemoveIndex;

  public SpawnListController()
  {
    ResetRemoveIndex();

    // TODO: 似たようなコードなので修正するべき？
    OnParamUpdated += (param, index) => {
      ResetRemoveIndex();
    };
    OnClickAddGameObjectButton += () => {
      ResetRemoveIndex();
    };
    OnClickRemoveGameObjectButton += (index) => {
      ResetRemoveIndex();
    };
    OnClickSelectGameObjectButton += (index) => {
      ResetRemoveIndex();
    };
    OnSelectedIndexChanged += (index) => {
      ResetRemoveIndex();
    };
  }

  public void Display(SpawnListObject.EditorParam[] parameters)
  {
    EditorGUILayout.LabelField("Spawn Object");

    if (parameters != null) {
      int index = 0;
      foreach(var param in parameters) {
        var spawnController = new SpawnController();
        AddEvent(spawnController, index);
        var spawnCount = SpawnObject.GetSpawnObjectAt(index).spawnCount;
        spawnController.Display(param.name, param.sprite, SpawnObject.GetSpawnObjectAt(index).GetRect(), spawnCount);
//        ObjectGroup(param, index);
        EditorGUILayout.Space();
        index++;
      } 
    }
    AddButton();
  }

  void AddEvent(SpawnController spawnController, int index)
  {
    spawnController.OnSelectedSpawnControllerChanged += () => {
      Debug.Log("select: " + index);
      OnSelectedIndexChanged(index);
    };
    spawnController.OnSpawn += (rect) => {
      var spawnObject = SpawnObject.GetSpawnObjectAt(index);
      var obj = spawnObject.Spawn(rect, SpawnObject.GetSpawnObjectAt(index).spawnBgObjects);
      if (obj) spawnObject.AddSpawnedObject(obj);
    };
    spawnController.OnNameChanged += (name) => {
      var param = new SpawnListObject().GetEditorParamAt(index);
      param.name = name;
      OnParamUpdated(param, index);
    };
    spawnController.OnSpawnNumChanged += (num) => {
      SpawnObject.GetSpawnObjectAt(index).spawnCount = num;
    };
  }

  public void ObjectGroup(SpawnListObject.EditorParam param, int index)
  {
    using(new EditorGUILayout.HorizontalScope(GUI.skin.box)) {
      using(new EditorGUILayout.VerticalScope(
        GUI.skin.box,
        new [] {
          GUILayout.Width(100), GUILayout.Height(100)
        })) {
        var newSprite = CustomEditorGUILayout.Field<Sprite>(param.sprite, GUILayout.Width(96), GUILayout.Height(96));
        if (newSprite != param.sprite) {
          param.sprite = newSprite;
          OnParamUpdated(param, index);
        }
      }

      using(new EditorGUILayout.VerticalScope()) {
        using(new EditorGUILayout.HorizontalScope())
        {
          if (GUILayout.Button("Select")) {
            OnClickSelectGameObjectButton(index);
          }

          if (checkRemoveIndex == index) {
            if (GUILayout.Button("Really?")) {
              OnClickRemoveGameObjectButton(index);
            }
          }
          else {
            if (GUILayout.Button("Remove")) {
              checkRemoveIndex = index;
            }
          }
        }

        var newName = EditorGUILayout.TextField("Name", param.name);
        if (newName != param.name) {
          param.name = newName;
          OnParamUpdated(param, index);
        }
      };
    }
  }

  public void ResetRemoveIndex()
  {
    checkRemoveIndex = -1;
  }

  public void AddButton()
  {
    if (GUILayout.Button("Add SpawnObject")) {
      OnClickAddGameObjectButton();
    }
  }
}
