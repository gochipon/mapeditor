﻿using UnityEngine;
using UnityEditor;

public class MapController
{
  public delegate void SpriteChangeHandler(Sprite sprite);
  public delegate void SimpleEventHandler();
  public delegate void NameChangeHander(string name);
  public delegate void FileCreateHandler(string filename);
  public event SpriteChangeHandler OnSpriteChanged;
  public event SimpleEventHandler OnClickGameObjectButton;
  public event NameChangeHander OnNameChanged;
  public event FileCreateHandler OnClickGenerateJsonButton;

  string filename;

  public void Display(Sprite sprite, string name)
  {
    EditorGUILayout.LabelField("Map");
    EditorGUI.indentLevel++;
    using(new EditorGUILayout.HorizontalScope(GUI.skin.box)) {
      var newSprite = CustomEditorGUILayout.Field<Sprite>(sprite, GUILayout.Width(96), GUILayout.Height(96));

      if (newSprite != sprite) {
        OnSpriteChanged(newSprite);
      }

      using(new EditorGUILayout.VerticalScope()) {
        if (GUILayout.Button("Select")) {
          OnClickGameObjectButton();
        }
        using(new EditorGUILayout.HorizontalScope()) {
          filename = EditorGUILayout.TextField("生成するJSONファイル名", filename);
          if (GUILayout.Button("Generate JSON")) {
            OnClickGenerateJsonButton(filename);
          }
        }
        EditorGUILayout.Space();
        var newName = EditorGUILayout.TextField(name);
        if (newName != name) {
          OnNameChanged(newName);
        }
      }
    }
    EditorGUI.indentLevel--;
    EditorGUILayout.Space();
  }
}
