﻿using UnityEngine;
using UnityEditor;
using System;

public class EditorBgObjectController
{
  public delegate void EditorParamUpdateEventHandler(EditorBgObject.EditorParam param, int index);
  public delegate void GameObjectSelectEventHandler(int index);
  public delegate void SimpleEventHandler();
  public event EditorParamUpdateEventHandler OnParamUpdated;
  public event SimpleEventHandler OnClickAddGameObjectButton;
  public event GameObjectSelectEventHandler OnClickSelectGameObjectButton;
  public event GameObjectSelectEventHandler OnClickRemoveGameObjectButton;
  public event SimpleEventHandler OnSortLayerOrderToggleOn;
  public event SimpleEventHandler OnSortLayerOrderToggleOff;
  public event SimpleEventHandler OnSortLayerOrder;
  public event SimpleEventHandler OnSortLayerOrderOff;

  static int checkRemoveIndex;
  bool isSort;

  public EditorBgObjectController()
  {
    ResetRemoveIndex();

    // TODO: 似たようなコードなので修正するべき？
    OnParamUpdated += (param, index) => {
      ResetRemoveIndex();
    };
    OnClickAddGameObjectButton += () => {
      ResetRemoveIndex();
      if (isSort) {
        OnSortLayerOrder();
      }
    };
    OnClickRemoveGameObjectButton += (index) => {
      ResetRemoveIndex();
      if (isSort) {
        OnSortLayerOrder();
      }
    };
    OnClickSelectGameObjectButton += (index) => {
      ResetRemoveIndex();
    };
    OnSortLayerOrderToggleOn += () => {
      ResetRemoveIndex();
      OnSortLayerOrder();
    };
    OnSortLayerOrderToggleOff += () => {
      ResetRemoveIndex();
      OnSortLayerOrderOff();
    };
  }

  public void Display(EditorBgObject.EditorParam[] parameters)
  {
    if (isSort && Event.current.type == EventType.mouseUp) {
      OnSortLayerOrder();
    }
    using(new EditorGUILayout.HorizontalScope()) {
      EditorGUILayout.LabelField("BgObject");
      var _isSort = EditorGUILayout.ToggleLeft("Sort Layout Order", isSort);
      if (_isSort != isSort) {
        if (_isSort) {
          OnSortLayerOrderToggleOn();
        }
        else {
          OnSortLayerOrderToggleOff();
        }
      }
      isSort = _isSort;
    }
    // EditorGUI.indentLevel++;
    if (parameters != null) {
      int index = 0;
      foreach(var param in parameters) {
        ObjectGroup(param, index);
        EditorGUILayout.Space();
        index++;
      }
    }
    AddButton();
    // EditorGUI.indentLevel--;
  }

  public void ObjectGroup(EditorBgObject.EditorParam param, int index)
  {
    using(new EditorGUILayout.HorizontalScope(GUI.skin.box)) {
      using(new EditorGUILayout.VerticalScope(
        GUI.skin.box,
        new [] {
          GUILayout.Width(100), GUILayout.Height(100)
        })) {
        var newSprite = CustomEditorGUILayout.Field<Sprite>(param.sprite, GUILayout.Width(96), GUILayout.Height(96));
        if (newSprite != param.sprite) {
          param.sprite = newSprite;
          OnParamUpdated(param, index);
        }
      }

      using(new EditorGUILayout.VerticalScope()) {
        using(new EditorGUILayout.HorizontalScope())
        {
          if (GUILayout.Button("Select")) {
            OnClickSelectGameObjectButton(index);
          }

          if (checkRemoveIndex == index) {
            if (GUILayout.Button("Really?")) {
              OnClickRemoveGameObjectButton(index);

              if (isSort) {
                OnSortLayerOrder();
              }
            }
          }
          else {
            if (GUILayout.Button("Remove")) {
              checkRemoveIndex = index;
            }
          }
        }

        var newName = EditorGUILayout.TextField("Name", param.name);
        if (newName != param.name) {
          param.name = newName;
          OnParamUpdated(param, index);
        }

        var newOrder = EditorGUILayout.IntSlider("Order", param.orderInLayer, Int16.MinValue, Int16.MaxValue);
        if (newOrder != param.orderInLayer) {
          param.orderInLayer = newOrder;
          OnParamUpdated(param, index);
        }

        var newPosition = EditorGUILayout.Vector2Field("Position", param.position);
        if (newPosition != param.position) {
          param.position = newPosition;
          OnParamUpdated(param, index);
        }

        var newLife = EditorGUILayout.IntSlider("Life", param.life, 1, 99);
        if (newLife != param.life) {
          param.life = newLife;
          OnParamUpdated(param, index);
        }

        var newRate = EditorGUILayout.IntSlider("Rate", param.spawnRate, 1, 100);
        if (newRate != param.spawnRate) {
          param.spawnRate = newRate;
          OnParamUpdated(param, index);
        }
      };
    }
  }

  public void ResetRemoveIndex()
  {
    checkRemoveIndex = -1;
  }

  public void AddButton()
  {
    if (GUILayout.Button("Add BgObject")) {
      OnClickAddGameObjectButton();

      if (isSort) {
        OnSortLayerOrder();
      }
    }
  }
}
