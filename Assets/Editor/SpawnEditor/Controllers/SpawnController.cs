﻿using UnityEngine;
using UnityEditor;

public class SpawnController
{
  public delegate void SimpleHandler();
  public delegate void RectHandler(Rect rect);
  public delegate void SpriteChangeHandler(Sprite sprite);
  public delegate void IntChangeHandler(int index);
  public delegate void NameChangeHander(string name);
  public event RectHandler OnRectChanged;
  public event RectHandler OnSpawn;
  public event SpriteChangeHandler OnSpriteChanged;
  public event SimpleHandler OnSelectedSpawnControllerChanged;
  public event NameChangeHander OnNameChanged;
  public event IntChangeHandler OnSpawnNumChanged;

  public void Display(string name, Sprite sprite, Rect rect, int spawnNum)
  {
//    EditorGUILayout.LabelField("Spawn Object");
    EditorGUI.indentLevel++;
    using(new EditorGUILayout.HorizontalScope(GUI.skin.box)) {
      var newSprite = CustomEditorGUILayout.Field<Sprite>(sprite, GUILayout.Width(96), GUILayout.Height(96));
      if (newSprite != sprite) {
        OnSpriteChanged(newSprite);
      }

      using(new EditorGUILayout.VerticalScope()) {
        using(new EditorGUILayout.HorizontalScope()) {
          if (GUILayout.Button("Select")) {
            OnSelectedSpawnControllerChanged();
          }
          if (GUILayout.Button("Generate")) {
            OnSpawn(rect);
          }
        }
        var newName = EditorGUILayout.TextField("Name", name);
        if (newName != name) {
          OnNameChanged(newName);
        }
        EditorGUILayout.LabelField("リスポーン範囲");
        var newRect = new Rect();
        using(new EditorGUILayout.HorizontalScope()) {
          EditorGUIUtility.labelWidth = 1;
          EditorGUIUtility.fieldWidth = 1;
          EditorGUILayout.LabelField("X0", GUILayout.Width(32));
          newRect.xMin = EditorGUILayout.FloatField("", rect.xMin, GUILayout.MinWidth(32));
          EditorGUILayout.LabelField("Y0", GUILayout.Width(32));
          newRect.yMin = EditorGUILayout.FloatField("", rect.yMin, GUILayout.MinWidth(32));
          EditorGUIUtility.labelWidth = 0;
          EditorGUIUtility.fieldWidth = 0;
        }

        using(new EditorGUILayout.HorizontalScope()) {
          EditorGUIUtility.labelWidth = 1;
          EditorGUIUtility.fieldWidth = 1;
          EditorGUILayout.LabelField("X1", GUILayout.Width(32));
          newRect.xMax = EditorGUILayout.FloatField("", rect.xMax, GUILayout.MinWidth(32));
          EditorGUILayout.LabelField("Y1", GUILayout.Width(32));
          newRect.yMax = EditorGUILayout.FloatField("", rect.yMax, GUILayout.MinWidth(32));
          EditorGUIUtility.labelWidth = 0;
          EditorGUIUtility.fieldWidth = 0;
        }
        if (newRect != rect) {
          OnRectChanged(newRect);
        }

        var newNum = EditorGUILayout.IntSlider("スポーン数", spawnNum, 0, 99);
        if (newNum != spawnNum) {
          OnSpawnNumChanged(newNum);
        }
      }
    }

    EditorGUI.indentLevel--;
    EditorGUILayout.Space();
  }
}
