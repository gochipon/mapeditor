﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

public class EditorBgObject
{
  public struct EditorParam {
    public string name;
    public Vector2 position;
    public int life;
    public int spawnRate;
    public int orderInLayer;
    public Sprite sprite;
    public GameObject original {get; private set;}

    public EditorParam(GameObject gameObject)
    {
      if (gameObject != null) {
        name = gameObject.name;
        position = gameObject.transform.position;
        life = gameObject.GetComponent<BgObject>().life;
        spawnRate = gameObject.GetComponent<BgObject>().spawnRate;
        orderInLayer = gameObject.GetComponent<SpriteRenderer>().sortingOrder;
        sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
      }
      else {
        var bgObject = new BgObject();
        name = null;
        position = Vector2.zero;
        life = bgObject.life;
        spawnRate = bgObject.spawnRate;
        orderInLayer = 0;
        sprite = null;
      }
      original = gameObject;
    }
  }

  const string TAG = "BgObject0"; 

  List<GameObject> gameObjects;

  SpriteRenderer renderer;
  BgObject bgObject;

  public EditorBgObject()
  {
    Sync();
  }

  private GameObject CreateGameObject()
  {
    return GetSpawnObject().AddGameObject();
  }

  public SpawnObject GetSpawnObject()
  {
    return SpawnEditorWindow.Instance.spawnObject;
  }

  public GameObject[] GetBgObjects()
  {
    return GetSpawnObject().spawnBgObjects;
  }

  public GameObject GetGameObjectAt(int index)
  {
    return gameObjects == null || index < 0 || index > (gameObjects.Count - 1) ?
      null : gameObjects[index];
  }

  public void RemoveGameObjectAt(int index)
  {
    GetSpawnObject().RemoveGameObjectAt(index);
    Sync();
 }

  public EditorParam[] GetEditorParams()
  {
    List<EditorParam> paramList = new List<EditorParam>();
    int index = 0;
    foreach (var obj in gameObjects) {
      paramList.Add(GetEditorParamAt(index));
      index++;
    }
    return paramList.ToArray();
  }

  public EditorParam GetEditorParamAt(int index)
  {
    return new EditorParam(GetGameObjectAt(index));
  }

  public void SetEditorParam(EditorParam param, int index)
  {
    var obj = GetGameObjectAt(index);

    renderer = obj.GetComponent<SpriteRenderer>();
    if (renderer.sprite != param.sprite) {
      // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
      Undo.RecordObject(renderer, "Sprite");
      renderer.sprite = param.sprite;
    }
    if (obj.name != param.name) {
      // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
      Undo.RecordObject(obj, "Name");
      obj.name = param.name;
    }
    if (obj.transform.position != new Vector3(param.position.x, param.position.y)) {
      // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
      Undo.RecordObject(obj.transform, "Position");
      obj.transform.position = param.position;
    }
    bgObject = obj.GetComponent<BgObject>();
    if (bgObject.life != param.life) {
      // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
      Undo.RecordObject(bgObject, "Life");
      bgObject.life = param.life;
    }
    if (bgObject.spawnRate != param.spawnRate) {
      // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
      Undo.RecordObject(bgObject, "Rate");
      bgObject.spawnRate = param.spawnRate;
    }
    if (renderer.sortingOrder != param.orderInLayer) {
      // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
      Undo.RecordObject(renderer, "Order in Layer");
      renderer.sortingOrder = param.orderInLayer;
    }
  }

  public Sprite GetSpriteAt(int index)
  {
    var obj = GetGameObjectAt(index);
    if (obj == null) return null;
    var renderer = obj.GetComponent<SpriteRenderer>();
    if (renderer == null) return null;
    return renderer.sprite;  
  }

  public void SetSprite(Sprite sprite, int index)
  {
    var obj = GetGameObjectAt(index);
    if (obj == null) return;
    obj.GetComponent<SpriteRenderer>().sprite = sprite;
  }

  public void AddGameObject()
  {
    CreateGameObject();
    Sync();
  }

  public void SortLayerOrder()
  {
    gameObjects.Sort((a, b) => {
      var o1 = a.GetComponent<SpriteRenderer>().sortingOrder;
      var o2 = b.GetComponent<SpriteRenderer>().sortingOrder;
      return o1.CompareTo(o2);
    });
  }

  public void Sync()
  {
    gameObjects = GetBgObjects().ToList();
  }
}
