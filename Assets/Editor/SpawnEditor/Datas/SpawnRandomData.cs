﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SpawnRandomData
{
  [Serializable]
  public struct Range
  {
    // LitJSONでエラーになるためここではdoubleにする
    public double x0,y0,x1,y1;

    public Range(Rect rect)
    {
      x0 = rect.xMin;
      y0 = rect.yMin;
      x1 = rect.xMax;
      y1 = rect.yMax;
    }
  }

  [Serializable]
  public struct GenerateObject
  {
    public int index;
    public int rate;
  }

  public Range range;
  public GenerateObject[] generateObjects;
  public int num;
}
