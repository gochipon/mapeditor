﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MapData
{
  public BackgroundData background;
  public MapObjectData[] mapObjects;
  public int[][] spawnObjects;
  public SpawnRandomData[] spawnRandom;
  public SpawnPositionData[] spawnPosition;
}
