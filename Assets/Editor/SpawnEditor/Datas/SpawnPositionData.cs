﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SpawnPositionData
{
  public struct GenerateObject
  {
    public int index;
  }

  public struct Position
  {
    public double x,y;
  }

  public Position position;
  public GenerateObject generateObject;
}
