﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MapObjectData
{
  public string prefab;
  public string sprite;
}
