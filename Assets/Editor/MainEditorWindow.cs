﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LitJson;
using System.Linq;

public class MainEditorWindow : EditorWindow
{
  static MainEditorWindow window;

  static int _selectedIndex;

  public static int selectedIndex
  {
    get
    {
      return _selectedIndex;
    }
    private set
    {
      _selectedIndex = value;
    }
  }

  MapObject _mapObject;

  MapObject mapObject
  {
    get
    {
      return _mapObject = _mapObject ?? new MapObject();
    }
    set
    {
      _mapObject = value;
    }
  }

  MapController _mapController;

  MapController mapController
  {
    get
    {
      return _mapController = _mapController ?? new MapController();
    }
    set
    {
      _mapController = value;
    }
  }

  SpawnListObject _spawnListObject;

  SpawnListObject spawnListObject
  {
    get
    {
      return _spawnListObject = _spawnListObject ?? new SpawnListObject();
    }
  }

  SpawnListController _spawnListController;

  SpawnListController spawnListController
  {
    get
    {
      return _spawnListController = _spawnListController ?? new SpawnListController();
    }
    set
    {
      _spawnListController = value;
    }
  }

  [MenuItem("MapEditor/MainWindow", false, 1)]
  static void ShowWindow()
  {
#if DEBUG
    window = null;
#endif

    if (window == null) {
      window = CreateInstance<MainEditorWindow>();

      window.ctor();
    }

    // 常に手前状態で表示する
    window.ShowUtility();
  }

  void ctor()
  {
    mapController.OnClickGameObjectButton += () => {
      Selection.activeGameObject = mapObject.GetGameObject();
    };
    mapController.OnClickGenerateJsonButton += (filename) => {
      GenerateJson(filename);
    };
    spawnListController.OnSelectedIndexChanged += (index) => {
      var spawnEditorWindow = SpawnEditorWindow.Instance;
      spawnEditorWindow.selectedIndex = index;
      spawnEditorWindow.ShowUtility();
      spawnEditorWindow.Repaint();
      Selection.activeGameObject = spawnListObject.GetGameObjectAt(index);
//      Debug.Log("repainted");
    };
  }

  Vector2 scrollPosition;

  void OnGUI() {
    scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, false, false);

    DisplayMap();
    DisplaySpawn();

    EditorGUILayout.EndScrollView();
  }

  void DisplayMap()
  {
    mapController.Display(mapObject.GetSprite(), mapObject.GetGameObject().name);
  }

  void DisplaySpawn()
  {
    spawnListController.Display(spawnListObject.GetEditorParams());
  }

  // TODO: データ関連メソッドを別の場所に移動させる

  void GenerateJson(string filename)
  {
    if (filename == null || filename == "") {
      Debug.Log("cannot create json file: filename is null or empty.");
      return;
    }

    var json = JsonMapper.ToJson(GenerateMapData());
    Debug.Log("GeneratedJson");
    try {
      var fileInfo = new FileInfo(Application.dataPath + Path.DirectorySeparatorChar + filename + ".json");
      using(var writer = new StreamWriter(fileInfo.OpenWrite(), Encoding.UTF8))
      {
        writer.Write(json);
      }
    }
    catch (IOException e) {
      Debug.LogError(e);
    }
  }

  MapData GenerateMapData()
  {
    var mapData = new MapData();
    mapData.background = GenerateBackground();
    GenerateObjects(ref mapData.mapObjects, ref mapData.spawnObjects);
    mapData.spawnRandom = GenerateSpawnRandom();
    mapData.spawnPosition = GenerateSpawnPosition(mapData.mapObjects);
    return mapData;
  }

  BackgroundData GenerateBackground()
  {
    var background = new BackgroundData();
    background.path = GetPath(mapObject.GetGameObject().GetComponent<SpriteRenderer>().sprite);
    return background;
  }

  void GenerateObjects(ref MapObjectData[] mapObjects, ref int[][] spawnObjects)
  {
    var mapObjectList = new List<MapObjectData>();
    var spawnObjectList = new List<int[]>();
    for(var i = 0; i < SpawnObject.GetSpawnObjectCount(); i++) {
      var objs = SpawnObject.GetSpawnObjectAt(i).spawnBgObjects;
      for(var j = 0; j < objs.Length; j++) {
        var obj = objs[j];
        var prefabPath = GetPath(obj);
        var spritePath = GetPath(obj.GetComponent<SpriteRenderer>().sprite);
        if (!mapObjectList.Exists(o => o.prefab == prefabPath && o.sprite == spritePath)) {
          var mapObjectData = new MapObjectData();
          mapObjectData.prefab = prefabPath;
          mapObjectData.sprite = spritePath;
          mapObjectList.Add(mapObjectData);
        }
        // TODO: 同じデータが複数作成される場合がある問題の解消
        spawnObjectList.Add(new int[]{mapObjectList.FindIndex(o => o.prefab == prefabPath && o.sprite == spritePath)});
      }
    }
    mapObjects = mapObjectList.ToArray();
    spawnObjects = spawnObjectList.ToArray();
  }

  SpawnRandomData[] GenerateSpawnRandom()
  {
    var spawnRandomList = new List<SpawnRandomData>();
    var c = 0;
    for(var i = 0; i < SpawnObject.GetSpawnObjectCount(); i++) {
      var spawnRandom = new SpawnRandomData();
      var spawnObject = SpawnObject.GetSpawnObjectAt(i);
      spawnRandom.range = new SpawnRandomData.Range(spawnObject.GetRect());
      spawnRandom.generateObjects = GenerateGenerateObjects(spawnObject, c);
      c += spawnRandom.generateObjects.Length;
      spawnRandom.num = spawnObject.spawnCount;
      spawnRandomList.Add(spawnRandom);
      Debug.Log(LitJson.JsonMapper.ToJson(spawnRandomList.ToArray()));
    }
    return spawnRandomList.ToArray();
  }

  SpawnRandomData.GenerateObject[] GenerateGenerateObjects(SpawnObject spawnObject, int startCount)
  {
    var generateObjectList = new List<SpawnRandomData.GenerateObject>();
    foreach(var obj in spawnObject.spawnBgObjects) {
      var generateObject = new SpawnRandomData.GenerateObject();
      generateObject.index = startCount++;
      generateObject.rate = obj.GetComponent<BgObject>().spawnRate;
      generateObjectList.Add(generateObject);
    }
    return generateObjectList.ToArray();
  }

  SpawnPositionData[] GenerateSpawnPosition(MapObjectData[] mapObjects)
  {
    var spawnPositionList = new List<SpawnPositionData>();
    for(int i = 0; i < 5; i++) {
      var objs = GameObject.FindGameObjectsWithTag("BgObject" + i + "Spawned");
      foreach(var obj in objs) {
        var spawnPosition = new SpawnPositionData();
        spawnPosition.position.x = obj.transform.position.x;
        spawnPosition.position.y = obj.transform.position.y;
        spawnPosition.generateObject.index = FindGenerateObjectIndex(mapObjects, obj);
        spawnPositionList.Add(spawnPosition);
      }
    }
    return spawnPositionList.ToArray();
  }

  int FindGenerateObjectIndex(MapObjectData[] mapObjects, GameObject obj)
  {
//    Debug.Log("jiugyftdr");
//    Debug.Log(GetPath((obj)));
//    Debug.Log(GetPath(PrefabUtility.FindPrefabRoot(obj)));
//    Debug.Log(GetPath(PrefabUtility.FindRootGameObjectWithSameParentPrefab(obj)));
//    Debug.Log(GetPath(PrefabUtility.FindValidUploadPrefabInstanceRoot(obj)));
//    Debug.Log(GetPath(PrefabUtility.GetPrefabObject(obj)));
//    Debug.Log(GetPath(PrefabUtility.GetPrefabParent(obj)));
    var prefabPath = GetPath(PrefabUtility.GetPrefabParent(obj));
//    Debug.Log("prefab path: " + prefabPath);
    var spritePath = GetPath(obj.GetComponent<SpriteRenderer>().sprite);
    return mapObjects.ToList<MapObjectData>().FindIndex(o => o.prefab == prefabPath && o.sprite == spritePath);
  }

  string GetPath(Object obj)
  {
    return AssetDatabase.GetAssetPath(obj);
  }
}
