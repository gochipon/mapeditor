﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class SpawnEditorWindow : EditorWindow
{
  static SpawnEditorWindow window;

  public static SpawnEditorWindow Instance
  {
    get
    {
      if (window == null) {
        window = CreateInstance<SpawnEditorWindow>();
        window.ctor();
      }
      return window;
    }
  }

//  MapObject _mapObject;
//  MapObject mapObject
//  {
//    get
//    {
//      return _mapObject = _mapObject ?? new MapObject();
//    }
//    set
//    {
//      _mapObject = value;
//    }
//  }
//
//  MapController _mapController;
//  MapController mapController
//  {
//    get
//    {
//      return _mapController = _mapController ?? new MapController();
//    }
//    set
//    {
//      _mapController = value;
//    }
//  }

  SpawnController _spawnController;

  public SpawnController spawnController
  {
    get
    {
      return _spawnController = _spawnController ?? new SpawnController();
    }
    set
    {
      _spawnController = value;
    }
  }

  int _selectedIndex;

  public int selectedIndex
  {
    get
    {
      return _selectedIndex;
    }
    set
    {
      _selectedIndex = value;
      editorBgObject.Sync();
    }
  }

  public SpawnObject spawnObject
  {
    get
    {
      return SpawnObject.GetSpawnObjectAt(selectedIndex);
    }
  }

  EditorBgObject _editorBgObject;

  EditorBgObject editorBgObject
  {
    get
    {
      return _editorBgObject = _editorBgObject ?? new EditorBgObject();
    }
    set
    {
      _editorBgObject = value;
    }
  }

  EditorBgObjectController _editorBgObjectController;

  EditorBgObjectController editorBgObjectController
  {
    get
    {
      return _editorBgObjectController = _editorBgObjectController ?? new EditorBgObjectController();
    }
    set
    {
      _editorBgObjectController = value;
    }
  }

  SpawnListObject _spawnListObject;

  SpawnListObject spawnListObject
  {
    get
    {
      return _spawnListObject = _spawnListObject ?? new SpawnListObject();
    }
    set
    {
      _spawnListObject = value;
    }
  }

  SpawnListController _spawnListController;

  SpawnListController spawnListController
  {
    get
    {
      return _spawnListController = _spawnListController ?? new SpawnListController();
    }
    set
    {
      _spawnListController = value;
    }
  }

  EditorBgObject.EditorParam param;

  Vector2 scrollPosition;

  [MenuItem("MapEditor/SpawnEditorWindow")]
  static void ShowWindow()
  {
#if DEBUG
    window = null;
#endif

    // 常に手前状態で表示する
    Instance.ShowUtility();
  }

  void ctor()
  {
//    mapController.OnSpriteChanged += sprite => {
//      Debug.Log("sprite updated");
//      mapObject.SetSprite(sprite);
//    };
//    mapController.OnClickGameObjectButton += () => {
//      Selection.activeGameObject = mapObject.GetGameObject();
//    };
//    mapController.OnNameChanged += name => {
//      mapObject.GetGameObject().name = name;
//    };
    editorBgObjectController.OnParamUpdated += (param, index) => {
      editorBgObject.SetEditorParam(param, index);
    };
    editorBgObjectController.OnClickAddGameObjectButton += () => {
      Debug.Log("append game object");
      editorBgObject.AddGameObject();
    };
    editorBgObjectController.OnClickSelectGameObjectButton += index => {
      Selection.activeGameObject = editorBgObject.GetGameObjectAt(index);
    };
    editorBgObjectController.OnClickRemoveGameObjectButton += index => {
      editorBgObject.RemoveGameObjectAt(index);
    };
    editorBgObjectController.OnSortLayerOrder += () => {
      editorBgObject.SortLayerOrder();
    };
    editorBgObjectController.OnSortLayerOrderOff += () => {
      editorBgObject.Sync();
    };
    spawnController.OnRectChanged += (rect) => {
      spawnObject.SetRect(rect);
    };
    spawnController.OnSpawn += (rect) => {
      var obj = spawnObject.Spawn(rect, editorBgObject.GetBgObjects());
      if (obj) spawnObject.AddSpawnedObject(obj);
    };
    spawnController.OnSpriteChanged += sprite => {
      Debug.Log("sprite updated");
      spawnObject.SetSprite(sprite);
    };
    spawnController.OnSpawnNumChanged += (num) => {
      spawnObject.spawnCount = num;
    };
    spawnController.OnSelectedSpawnControllerChanged += () => {
      Repaint();
    };
    spawnListController.OnSelectedIndexChanged += (index) => {
      selectedIndex = index;
    };
    spawnListController.OnClickAddGameObjectButton += () => {
      spawnListObject.AddGameObject();
    };
  }

  void OnGUI()
  {
    scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, false, false);

    // http://answers.unity3d.com/questions/168403/custom-inspector-label-width.html
    EditorGUIUtility.labelWidth = 64;

//    DisplayMapControl();
    DisplaySpawn();
    DisplayEditorBgObjectControl();
    EditorGUILayout.EndScrollView();

//    param.sprite = CustomEditorGUILayout.Field<Sprite>(param.sprite, GUILayout.MaxWidth(96), GUILayout.MinHeight(96));
//
//    param.position = EditorGUILayout.Vector2Field("Position", param.position);
//
//    EditorGUILayout.Space();
//
//    using(new EditorGUILayout.HorizontalScope())
//    {
//      param.name = EditorGUILayout.TextField("Name", param.name);
//      param.life = EditorGUILayout.IntSlider("Life", param.life, 1, 99);
//    }
//
//    EditorGUIUtility.labelWidth = 0;
//    param.orderInLayer = EditorGUILayout.IntField("Order in Layer", param.orderInLayer);
//
//    EditorGUILayout.Space();
  }

//  private void DisplayMapControl()
//  {
//    Debug.Log(mapController);
//    Debug.Log(mapObject);
//    mapController.Display(mapObject.GetSprite(), mapObject.GetGameObject().name);
//  }

  private void DisplaySpawn()
  {
    EditorGUILayout.LabelField("Spawn Object");
    spawnController.Display(spawnObject.GetName(), spawnObject.GetSprite(), spawnObject.GetRect(), spawnObject.spawnCount);
  }

  private void DisplayEditorBgObjectControl()
  {
    editorBgObjectController.Display(editorBgObject.GetEditorParams());

    // Sprite[] sprites = new Sprite[editorBgObject.Count()];
    // for (int i = 0; i < editorBgObject.Count(); i ++) {
    //   sprites[i] = editorBgObject.GetSpriteAt(i);
    // }
    // editorBgObjectController.Display(sprites);
  }
}
