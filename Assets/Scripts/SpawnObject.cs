﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using System.IO;

[RequireComponent(typeof(SpriteRenderer))]
public class SpawnObject : MonoBehaviour
{
  static readonly string Tag = "Spawn";

  List<GameObject> spawnedObjects = new List<GameObject>();

  public GameObject[] spawnBgObjects;

  public int spawnCount;

  void Start()
  {
    spawnedObjects = new List<GameObject>();
    Spawn();
  }

  public static GameObject CreateSpawnObject()
  {
    var obj = new GameObject("Spawn Object");
    obj.AddComponent<SpawnObject>();
    obj.tag = Tag;
    return obj;
  }

  public static int GetSpawnObjectCount()
  {
    return GameObject.FindGameObjectsWithTag(Tag).Count();
  }

  public static SpawnObject GetSpawnObjectAt(int index)
  {
    return GameObject.FindGameObjectsWithTag(Tag)[index].GetComponent<SpawnObject>();
  }

  public Sprite GetSprite()
  {
    return gameObject.GetComponent<SpriteRenderer>().sprite;
  }

  public void SetSprite(Sprite sprite)
  {
    var renderer = gameObject.GetComponent<SpriteRenderer>();
    // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
    Undo.RecordObject(renderer, "Sprite");
    renderer.sprite = sprite;
  }

  public Rect GetRect()
  {
    var rect = new Rect();
    rect.min = GetRectMin();
    rect.max = GetRectMax();
    return rect;
  }

  public Vector2 GetRectMin()
  {
    var scale = gameObject.transform.localScale;
    return gameObject.transform.position - scale / 2;
  }

  public Vector2 GetRectMax()
  {
    var scale = gameObject.transform.localScale;
    return gameObject.transform.position + scale / 2;
  }

  public void SetRect(Rect rect)
  {
    // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
    Undo.RecordObject(gameObject.transform, "Rect");
    gameObject.transform.position = rect.center;
    gameObject.transform.localScale = rect.size;
  }

  public string GetName()
  {
    return gameObject.name;
  }

  public void AddSpawnedObject(GameObject spawnedObject)
  {
    spawnedObjects.Add(spawnedObject);
  }

  public void RemoveGameObjectAt(int index)
  {
    var list = spawnBgObjects.ToList();
    list.RemoveAt(index);
    spawnBgObjects = list.ToArray();
  }

  public GameObject AddGameObject()
  {
    // FIXME: Undoした時にNullReferenceExceptionが発生しないようにする
    Undo.RecordObject(this, "Add Spawn BgObject");
    var list = spawnBgObjects.ToList();
    var obj = CreateGameObject();
    list.Add(obj);
    spawnBgObjects = list.ToArray();
    return obj;
  }

  public GameObject CreateGameObject()
  {
    var obj = new GameObject();

    obj.AddComponent<SpriteRenderer>();
    obj.AddComponent<BgObject>();
    obj.tag = "BgObject";
    var filePath = GetAssetsFilePath("Assets/Prefab/", "NewPrefab", ".prefab");
    var prefabObj = PrefabUtility.CreatePrefab(filePath, obj);
    UnityEditor.AssetDatabase.SaveAssets();
    PrefabUtility.ConnectGameObjectToPrefab(obj, prefabObj);
    GameObject.DestroyImmediate(obj);

    return (GameObject)PrefabUtility.InstantiatePrefab(prefabObj);
  }

  static string GetAssetsFilePath(string path, string filename, string ext)
  {
    var filePath = path + filename + ext;

    if (!File.Exists(filePath)) return filePath;

    for(var i = 1; i > 0; i++)
    {
      filePath = path + filename + " (" + i + ")" + ext;

      if (!File.Exists(filePath)) return filePath;
    }

    throw new IOException("" + filename + " is too created in " + path);
  }

  void Spawn()
  {
    var rect = GetRect();

    while (spawnCount > 0) {
      var obj = Spawn(rect, spawnBgObjects);
      if (obj) {
        spawnCount--;
      }
      else {
        Debug.Log("スポーン失敗");
      }
    }
  }

  bool IsAllowSpawn(Rect spawnRect)
  {
    return true;
//    return spawnedObjects.All(o => 
//      !new Rect(o.transform.position, objectSize).Overlaps(spawnRect)
//    );
  }

  public GameObject Spawn(Rect rect, GameObject[] bgObjects)
  {
    if (bgObjects == null || bgObjects.Length == 0) return null;

    var x = Random.Range(rect.xMin, rect.xMax);
    var y = Random.Range(rect.yMin, rect.yMax);
    var bgObject = bgObjects[Random.Range(0, bgObjects.Length)];
//    var spawnRect = new Rect(new Vector2(x, y), objectSize);
//
//    if (IsAllowSpawn(spawnRect)) {
      var obj = (GameObject)PrefabUtility.InstantiatePrefab(bgObject);
      obj.transform.position = new Vector2(x, y);
      obj.tag += "Spawned";
      obj.SetActive(true);
      spawnedObjects.Add(obj);
      return obj;
//    }
//
//    Debug.Log("生成できないところに生成しようとしました (" + x + ", " + y + ")");
//    return Spawn(rect, bgObjects, loopCount + 1);
  }
}
