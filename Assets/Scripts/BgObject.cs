﻿using UnityEngine;
using System.Collections;

public class BgObject : MonoBehaviour
{
  //破壊エフェクト
  public GameObject particlePrefab;
  //耐久値
  public int life = 1;
  //スポーンする確率(比)
  public int spawnRate = 100;

  private SpriteRenderer spriteRenderer;
  private MeshRenderer meshRenderer;
}